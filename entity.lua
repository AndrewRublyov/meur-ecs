local currentFolder = (...):match("(.+)[/.].*$")
local class = require(currentFolder .. "/middleclass.middleclass")
local Component = require(currentFolder .. "/component")

local Entity = class("Entity")
---------------------------------------------
-- TODO TODO TODO: Fix #8, #9 lines
---------------------------------------------
function Entity:initialize(entityData)
    self.components = {}
    self.children = {}
    self.name = entityData.name or "unnamed"

    if entityData.components and type(entityData.components) == "table" then
        for k, v in ipairs(entityData.components) do
            local component = Component(v.path, v.properties)
            component:execute()
            table.insert(self.components, component)
        end
    end

    if entityData.children and type(entityData.children) == "table" then
        for k, v in ipairs(entityData.children) do
            local entity = Entity(v)
            table.insert(self.children, entity)
        end
    end
end

-- TODO: Write import functions in import module

function Entity:call(name, ...)
    for k, v in ipairs(self.components) do
        v:call(name, ...)
    end
    for k, v in ipairs(self.children) do
        v:call(name, ...)
    end
end

-- Get components table with given name
-- Because of that many components may have same name
function Entity:getComponents(name)
    local result = {}
    for k, v in ipairs(self.components) do
        if v.name and v.name == name then
            table.insert(result, v)
        end
    end
    return result
end

-- Get first component with given name
function Entity:getComponent(name)
    for k, v in ipairs(self.components) do
        if v.name and v.name == name then
            return v
        end
    end
    return nil
end

-- If recursive, it finds on children of the children
function Entity:findChildByName(name, recursive)
    for k, v in ipairs(self.children) do
        if v.name and v.name == name then
            return v
        end
        if recursive then
            return v:findChildByName(name, true)
        end
    end
    return nil
end

return Entity

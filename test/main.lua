-- Relative paths support
package.path = "../?.lua;" .. package.path

require "../dark-errors/init"
Entity = require "../entity"
Component = require "../component"
Import = require "../import"

function love.load(arg)
    scene = Import.loadEntity('../entity-example-nofunc.lua')
    scene:call("load")
end

function love.update(dt)
    scene:call("update", dt)
end

function love.draw()
    scene:call("draw")
end

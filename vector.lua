Vector = {}

mt = {
    __call = function(a, x, y)
        local new = {
            x = x,
            y = y,

            unpack = function(self) return self.x, self.y end,

            copy = function() return Vector(self.x, self.y) end,

            set = function(self, x, y)
                self.x = x
                self.y = y
            end,

            sum = function(self, x, y)
                self.x = self.x + x
                self.y = self.y + y
            end,

            mul = function(self, x, y)
                self.x = self.x * x
                self.y = self.y * y
            end,

            div = function(self, x, y)
                self.x = self.x / x
                self.y = self.y / y
            end
        }

        return setmetatable(new, {
            __add = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return Vector(a.x + b.x, a.y + b.y)
                else
                    error('Vector can be + only to a vector')
                end
            end,

            __sub = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return Vector(a.x - b.x, a.y - b.y)
                else
                    error('Vector can be - only to a vector')
                end
            end,

            __mul = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return Vector(a.x * b.x, a.y * b.y)
                else
                    error('Vector can be * only to a vector')
                end
            end,

            __div = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return Vector(a.x * b.x, a.y * b.y)
                else
                    error('Vector can be / only to a vector')
                end
            end,

            __pow = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return Vector(a.x ^ b.x, a.y ^ b.y)
                else
                    error('Vector can be ^ only to a vector')
                end
            end,

            __unm = function(a, b)
                return Vector(-a.x, -a.y)
            end,

            __len = function(a)
                return math.sqrt(math.pow(a.x) + math.pow(a.y))
            end,

            __eq = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return a.x == b.x and a.y == b.y
                else
                    error('Vector can be == only to a vector')
                end
            end,

            __lt = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return #a < #b
                else
                    error('Vector can be < only to a vector')
                end
            end,

            __le = function(a, b)
                if type(a) == "table" and type(b) == "table" then
                    return #a <= #b
                else
                    error('Vector can be <= only to a vector')
                end
            end
        })
    end
}



return setmetatable(Vector, mt)

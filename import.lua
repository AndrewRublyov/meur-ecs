local currentFolder = (...):match("(.+)[/.].*$")

local class = require(currentFolder .. "/middleclass.middleclass")
local Env = require(currentFolder .. "/env")
local Entity = require(currentFolder .. "/entity")

local Import = class("Import")


function Import.static.loadEntity(path)
    local env = {}
    Env.configureSandbox(env)

    -- NOTE: Shortcut to manual table creation
    env.entity = function(name, components, children)
        local entity = {
            name = name,
            components = {},
            children = {}
        }
        if components and type(components) == "table" then
            for k, v in ipairs(components) do
                table.insert(entity.components, {
                    name = v.name,
                    path = v.path,
                    properties = v.properties
                })
            end
        end
        if children and type(children) == "table" then
            for k, v in ipairs(children) do
                table.insert(entity.children, env.entity(
                    v.name,
                    v.components,
                    v.children
                ))
            end
        end
        return entity
    end

    -- NOTE: Shortcut to manual table creation
    env.component = function(name, path, properties)
        local component = {
            name = name,
            path = path,
            properties = properties
        }

        return component
    end
    local loadedEntity = assert(loadfile(path))
    setfenv(loadedEntity, env)
    return Entity(loadedEntity())
end

function Import.static.loadComponent(path)

end

return Import

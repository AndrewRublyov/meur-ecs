local currentFolder = (...):match("(.+)[/.].*$")

local class = require(currentFolder .. "/middleclass.middleclass")
local Env = require(currentFolder .. "/env")

local Component = class("Component")
local Vector = require(currentFolder .. "/vector")

function Component:initialize(path, poperties)
    self.path = path
    self.executed = false
    self.env = { scriptProperties = {} }
    self.properties = properties or {}
end

-- TODO: Write import functions in import module

-- Executes and stores component in environment
function Component:execute()
    if self.executed then error("Component was already executed. Check the 'env' variable") end
    if not self.path or type(self.path) ~= "string" then
        error("Path to the script is invalid or not set: '"..self.path.."'")
    end
    Env.configureSandbox(self.env)
    self:initializeFunctions()
    local loadedScript = assert(loadfile(Component.getComponentPath(self.path)))
    setfenv(loadedScript, self.env)
    loadedScript()
    -- Load properties
    if self.properties and type(self.properties) == "table" then
        for k, v in pairs(self.properties) do
            rawset(self.env, k, v)
        end
    end
    self.executed = true
    return self.env
end

-- Finds full path to component shortname
function Component.static.getComponentPath(input)
    local withLua = input .. ".lua"

    -- 1 - user's game root folder
    if love.filesystem.exists(withLua) then return withLua end
    if love.filesystem.exists(input) then return input end

    -- 1 priority - component folder inside user's game folder
    if love.filesystem.exists("components/"..withLua) then return "components/"..withLua end
    if love.filesystem.exists("components/"..input) then return "components/"..input end

    -- 3 - builtin folder
    if love.filesystem.exists(currentFolder .. "/built-in/components/"..withLua) then return currentFolder .. "/built-in/components/"..withLua end
    if love.filesystem.exists(currentFolder .. "/built-in/components/"..input) then return currentFolder .. "/built-in/components/"..input end

    error("Can't find component '"..input.."'")
end

-- Events
function Component:initializeFunctions()
    -- Callbacks
    self.env.start = function() end
    self.env.update = function() end
    self.env.draw = function() end
    -- TODO: More callbacks

    -- Helper functions
    self.env.property = function(name, defaultValue, set, get)
        self.env.scriptProperties[name] = {name = name, default = defaultValue, set = set, get = get}
    end

    self.env.vector = Vector
    -- TODO: More helper functions



end

-- Calls function inside component
function Component:call(functionName, ...)
    self.env[functionName](...)
end

return Component

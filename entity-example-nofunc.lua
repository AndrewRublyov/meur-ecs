-- This is
--[[
return {
    ["name"] = "test_entity",
    ["components"] = {
        {
            ["name"] = "transform",
            ["path"] = "engine/transform.lua",
            ["properties"] = {
                {["x"] = 5, ["y"] = 10}
            }
        },
        {
            ["name"] = "foo",
            ["path"] = "game/foo.lua",
            ["properties"] = {
                {["bar"] = "marco" .. math.sin(100)}
            }
        }
    }
    --["children"]
}
]]
-- equals to
return
entity("test_entity", {
    component("transform", "transform", {
        x = 5,
        y = 10
    }),
    component("foo", "foo", {
        bar = "marco" .. math.sin(100)
    })
}, {
    entity("lil")
})

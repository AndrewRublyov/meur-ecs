-- Transformation properties
property("position", vector(0, 0), function(v) setPosition(v.x, v.y) end, function() return getPosition() end )
property("rotation", 0, function(v) setRotation(v) end, function() return getRotation() end )
property("scale", vector(1, 1), function(v) setScale(v.x, v.y) end, function() return getScale() end )

-- For local positioning
property("localPosition", vector(0, 0), function(v) setLocalPosition(v.x, v.y) end, function() return getLocalPosition() end )
property("localRotation", 0, function(v) setLocalRotation(v) end, function() return getLocalRotation() end )
property("localScale", vector(1, 1), function(v) setLocalScale(v.x, v.y) end, function() return getLocalScale() end )

_position = vector(0, 0)
_rotation = 0
_scale = vector(1, 1)

_localPosition = vector(0, 0)
_localRotation = 0
_localScale = vector(1, 1)

function start()

end

function toPositiveAngle(angle)
    return angle >= 0 and angle or angle + 360 * math.ceil(math.abs(angle) / 360)
end

--------------------------------------
-- Transform manipulation functions --
--------------------------------------

function setPosition(x, y)
    x = x or _position.x
    y = y or _position.y
    local diff_x = x - _position.x
    local diff_y = y - _position.y
    _position:set(x, y)
    _localPosition:sum(diff_x, diff_y)
end

function setRotation(angle)
    local diff = toPositiveAngle(angle) - toPositiveAngle(_rotation)
    _rotation = angle
    _localRotation = _localRotation + diff
end

function setScale(x, y)
    x = x or _scale.x
    y = y or _scale.y
    local diff_x = x / _scale.x
    local diff_y = y / _scale.y
    _scale:set(x, y)
    _localPosition:mul(diff_x, diff_y)
end


function setLocalPosition(x, y)
    x = x or _localPosition.x
    y = y or _localPosition.y
    local diff_x = x - _localPosition.x
    local diff_y = y - _localPosition.y
    _position:sum(diff_x, diff_y)
    _localPosition:set(x, y)
end

function setLocalRotation(angle)
    local diff = toPositiveAngle(angle) - toPositiveAngle(_rotation)
    _localRotation = angle
    _rotation = _rotation + diff
end

function setLocalScale(x, y)
    x = x or _localScale.x
    y = y or _localScale.y
    local diff_x = x / _localScale.x
    local diff_y = y / _localScale.y
    _localScale:set(x, y)
    _scale:mul(diff_x, diff_y)
end

function move(x, y)
    _position:sum(x, y)
    _localPosition:sum(x, y)
end

function rotate(angle)
    _rotation = toPositiveAngle(_rotation + angle)
    _localRotation = toPositiveAngle(_localRotation + angle)
end

function getPosition()
    return _position
end

function getRotation()
    return _rotation
end

function getScale()
    return _scale
end

function getLocalPosition()
    return _localPosition
end

function getLocalRotation()
    return _localRotation
end

function getLocalScale()
    return _localScale
end

function update()
    --[[matrix.translate(position.x, position.y)
	matrix.rotate(rotation/180*math.pi)
	matrix.scale(scale.x, scale.y)
    matrix.scale(1/scale.x, 1/scale.y)
	matrix.rotate(-rotation/180*math.pi)
	matrix.translate(-position.x, -position.y)]]
end

function beforeDraw()
    matrix.translate(_localPosition.x, _localPosition.y)
	matrix.rotate(_localRotation/180*math.pi)
	matrix.scale(_localScale.x, _localScale.y)
end

function afterDraw()
    matrix.scale(1/_localScale.x, 1/_localScale.y)
	matrix.rotate(-_localRotation/180*math.pi)
	matrix.translate(-_localPosition.x, -_localPosition.y)
end

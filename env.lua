local currentFolder = (...):match("(.+)[/.].*$")

local class = require(currentFolder .. "/middleclass.middleclass")

local Environment = class("Environment")

function Environment.static.configureSandbox(env)
    local envProxy = {}
    envProxy.__parent = getfenv(2)

    envProxy.__index = function(table, key)
        local localEnv = rawget(table, "__environment")
        local globalEnv = rawget(getmetatable(table), "__parent")
        -- If key exists in local environment - return it
        -- this trick is needed for Lua and Love2d modules support inside component
        if localEnv and localEnv[key] then
            return localEnv[key]
        elseif localEnv.scriptProperties and localEnv.scriptProperties[key] then
            return localEnv.scriptProperties[key].get()
        end
        -- Otherwise check for current environment
        if not globalEnv then return nil end
        return globalEnv[key]
    end

    envProxy.__newindex = function(table, key, value)
        local localEnv = rawget(table, "__environment")
        local globalEnv = rawget(getmetatable(table), "__parent")

        if not localEnv then
            rawset(table, "__environment", {})
            localEnv = rawget(table, "__environment")
        end

        if rawget(globalEnv, key) then
            error("You are not allowed to modify outer member '"..key.."' or create members with same name")
        end

        if localEnv.scriptProperties and localEnv.scriptProperties[key] then
            localEnv.scriptProperties[key].set(value)
        else
            rawset(localEnv, key, value)
        end
    end

    setmetatable(env, envProxy)
    return env
end

return Environment

-- Great thanks to pgimeno for this code :)

local lgorigin = love.graphics.origin
local lgscale = love.graphics.scale
local lgrotate = love.graphics.rotate
local lgshear = love.graphics.shear
local lgtranslate = love.graphics.translate

local matrix = {1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1}

local function getMatrix(t)
  if t == nil then
    -- Return a copy
    return {matrix[ 1], matrix[ 2], matrix[ 3], matrix[ 4],
            matrix[ 5], matrix[ 6], matrix[ 7], matrix[ 8],
            matrix[ 9], matrix[10], matrix[11], matrix[12],
            matrix[13], matrix[14], matrix[15], matrix[16]}
  end
  -- Assign the elements (enables reusing tables)
  for i = 1, 16 do t[i] = matrix[i] end
  return t
end

local function setMatrix(m)
	matrix = m or {	1,0,0,0,
					0,1,0,0,
					0,0,1,0,
					0,0,0,1}
end

local function origin()
  matrix[1 ] = 1  matrix[2 ] = 0  matrix[3 ] = 0  matrix[4 ] = 0
  matrix[5 ] = 0  matrix[6 ] = 1  matrix[7 ] = 0  matrix[8 ] = 0
  matrix[9 ] = 0  matrix[10] = 0  matrix[11] = 1  matrix[12] = 0
  matrix[13] = 0  matrix[14] = 0  matrix[15] = 0  matrix[16] = 1
  lgorigin()
end

local function scale(x, y)
  matrix[ 1] = matrix[ 1] * x  matrix[ 2] = matrix[ 2] * y
  matrix[ 5] = matrix[ 5] * x  matrix[ 6] = matrix[ 6] * y
  matrix[ 9] = matrix[ 9] * x  matrix[10] = matrix[10] * y
  matrix[13] = matrix[13] * x  matrix[14] = matrix[14] * y
  lgscale(x, y)
end

local function rotate(a)
  local c, s = math.cos(a), math.sin(a)
  matrix[ 1], matrix[ 2] = matrix[ 1]*c + matrix[ 2]*s, matrix[ 1]*-s + matrix[ 2]*c
  matrix[ 5], matrix[ 6] = matrix[ 5]*c + matrix[ 6]*s, matrix[ 5]*-s + matrix[ 6]*c
  matrix[ 9], matrix[10] = matrix[ 9]*c + matrix[10]*s, matrix[ 9]*-s + matrix[10]*c
  matrix[13], matrix[14] = matrix[13]*c + matrix[14]*s, matrix[13]*-s + matrix[14]*c
  lgrotate(a)
end

local function shear(x, y)
  matrix[ 1], matrix[ 2] = matrix[ 1] + matrix[ 2]*y, matrix[ 1]*x + matrix[ 2]
  matrix[ 5], matrix[ 6] = matrix[ 5] + matrix[ 6]*y, matrix[ 5]*x + matrix[ 6]
  matrix[ 9], matrix[10] = matrix[ 9] + matrix[10]*x, matrix[ 9]*y + matrix[10]
  matrix[13], matrix[14] = matrix[13] + matrix[14]*x, matrix[13]*y + matrix[14]
  lgshear(x, y)
end

local function translate(x, y)
  matrix[4 ] = matrix[4 ] + matrix[1 ]*x + matrix[2 ]*y
  matrix[8 ] = matrix[8 ] + matrix[5 ]*x + matrix[6 ]*y
  matrix[12] = matrix[12] + matrix[9 ]*x + matrix[10]*y
  matrix[16] = matrix[16] + matrix[11]*x + matrix[12]*y
  lgtranslate(x, y)
end

local function xform(matrix, x, y)
  return matrix[1]*x + matrix[2]*y + matrix[4], matrix[5]*x + matrix[6]*y + matrix[8]
end

return {getMatrix=getMatrix, setMatrix=setMatrix, xform = xform, origin=origin,
        scale=scale, rotate=rotate, shear=shear, translate=translate}
